<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://laranz.in
 * @since      1.0.0
 *
 * @package    Pagebridge
 * @subpackage Pagebridge/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Pagebridge
 * @subpackage Pagebridge/includes
 * @author     laranz <laranz.joe@gmail.com>
 */
class Pagebridge_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
