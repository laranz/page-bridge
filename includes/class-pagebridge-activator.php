<?php

/**
 * Fired during plugin activation
 *
 * @link       http://laranz.in
 * @since      1.0.0
 *
 * @package    Pagebridge
 * @subpackage Pagebridge/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Pagebridge
 * @subpackage Pagebridge/includes
 * @author     laranz <laranz.joe@gmail.com>
 */
class Pagebridge_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
