<?php

/**
 * Class for the Custom Registration Page.
 *
 * @link       http://laranz.in
 * @since      1.0.0
 *
 * @package    Pagebridge
 * @subpackage Pagebridge/includes
 */

/**
 *
 * This class defines all code necessary to run during the registration page..
 *
 * @since      1.0.0
 * @package    Pagebridge
 * @subpackage Pagebridge/includes
 * @author     laranz <laranz.joe@gmail.com>
 */
class Pagebridge_Registration {

	private $username;
    private $password;

    function __construct()
	{
	    add_shortcode('pb_registration_form', array($this, 'registration_shortcode'));
	    add_action( 'login_form_register', array( $this, 'redirect_to_custom_register' ) );
	}

	/**
	 * Redirects the user to the custom registration page instead
	 * of wp-login.php?action=register.
	 */
	public function redirect_to_custom_register() {
		if ( 'GET' == $_SERVER['REQUEST_METHOD'] ) {
			if ( is_user_logged_in() ) {
				$this->redirect_logged_in_user();
			} else {
				wp_redirect( home_url( 'registration-form' ) );
			}
		exit;
		}
	}

	public function render_register_form( ) {
		if ( is_user_logged_in() ) {
		    _e( 'You are already signed in.', 'personalize-login' );
		} elseif ( ! get_option( 'users_can_register' ) ) {
		    _e( 'Registering new users is currently not allowed.', 'personalize-login' );
		} else {
		    return $this->registration_form();
		}
	}

	public function registration_form()
    {
       ?>
         <form method="post" action="<?php echo esc_url($_SERVER['REQUEST_URI']); ?>">
            <div class="login-form">

                <div class="form-group">
                    <input name="reg_name" type="text" class="form-control login-field"
                           value="<?php echo(isset($_POST['reg_name']) ? $_POST['reg_name'] : null); ?>"
                           placeholder="<?php _e( 'Username.', 'pagebridge' ); ?>" id="reg-name" required/>
                    <label class="login-field-icon fui-user" for="reg-name"></label>
                </div>
 
                <div class="form-group">
                    <input name="reg_password" type="password" class="form-control login-field"
                           value="<?php echo(isset($_POST['reg_password']) ? $_POST['reg_password'] : null); ?>"
                           placeholder="<?php _e( 'Password.', 'pagebridge' ); ?>" id="reg-pass" required />
                    <label class="login-field-icon fui-lock" for="reg-pass"></label>
                </div>
 
 	            <input class="btn btn-submit" type="submit" name="reg_submit" value="<?php _e( 'Register.', 'pagebridge' ); ?>" />
 	        </div>
        </form>
    <?php
    }

    function validate_form()
    {
    	if ( empty($this->username) || empty($this->password) ) {
            return new WP_Error('field', 'Required form field is missing');
        }

        if (!validate_username($this->username)) {
            return new WP_Error('name_invalid', __( "Username is already taken.", "pagebridge" ));
        }
 
        if (strlen($this->username) < 4) {
            return new WP_Error('username_length', __( "Username too short. At least 4 characters is required", "pagebridge" ) );
        }

        if (strlen($this->password) < 5) {
            return new WP_Error('password', __( "Password length must be greater than 5", "pagebridge" ) );
        }
    }

    function register_user()
	{
	 
	    $userdata = array(
	    	'user_login' => esc_attr($this->username),
	        'user_pass' => esc_attr($this->password),
	    );
	 
	    if (is_wp_error($this->validate_form())) {
	        echo '<div class="validationError">';
	        echo '<strong>' . $this->validate_form()->get_error_message() . '</strong>';
	        echo '</div>';
	    } else {
	        $register_user = wp_insert_user($userdata);
	        if (!is_wp_error($register_user)) {
	            echo '<div class="validationSuccess">';
	            _e('<strong>Registration complete. Goto <a href="' . wp_login_url() . '">login page</a></strong>', 'pagebridge');
	            echo '</div>';
	            //Success Redirect to user page.
				$redirect_url = home_url( 'sample-page' );
				wp_redirect( $redirect_url );
				exit;
	        } else {
	            echo '<div class="validationError">';
	            echo '<strong>' . $register_user->get_error_message() . '</strong>';
	            echo '</div>';
	        }
	    }
	}

	function registration_shortcode() {
 
        ob_start();

        if (isset($_POST['reg_submit'])) {
        	$this->username = $_POST['reg_name'];
            $this->password = $_POST['reg_password'];

            $this->validate_form();
            $this->register_user();
        }
	
	    $this->render_register_form();
 	
 	    return ob_get_clean();
    }

}

new Pagebridge_Registration;