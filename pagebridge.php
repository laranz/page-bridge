<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://laranz.in
 * @since             1.0.0
 * @package           Pagebridge
 *
 * @wordpress-plugin
 * Plugin Name:       Page Bridge
 * Plugin URI:        http://pagebridge.com
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.0
 * Author:            laranz
 * Author URI:        http://laranz.in
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       pagebridge
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-pagebridge-activator.php
 */
function activate_pagebridge() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-pagebridge-activator.php';
	Pagebridge_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-pagebridge-deactivator.php
 */
function deactivate_pagebridge() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-pagebridge-deactivator.php';
	Pagebridge_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_pagebridge' );
register_deactivation_hook( __FILE__, 'deactivate_pagebridge' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-pagebridge.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_pagebridge() {

	$plugin = new Pagebridge();
	$plugin->run();

}
run_pagebridge();
